<?php

namespace Drupal\face_recognition;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for face_recognition plugins.
 */
abstract class FaceRecognitionPluginBase extends PluginBase implements ContainerFactoryPluginInterface, FaceRecognitionInterface, PluginFormInterface {

  /**
   * Full path to users profile picture.
   *
   * @var string
   */
  protected $referencePicture;

  /**
   * Full path to webcam picture.
   *
   * @var string
   */
  protected $inputPicture;

  /**
   * Messenger service to display messages on form.
   *
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Logger to register important events in Drupal log.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Translates module configuration.
   *
   * @var Drupal\Core\StringTranslation\TranslationManager
   */
  protected $translationManager;

  /**
   * Obtain a complete path of the files.
   *
   * @var Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  protected $streamWrapperManager;

  /**
   * Gets a temp file name.
   *
   * @var Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Manage module configuration.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Loads user picture file.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              Messenger $messenger,
                              LoggerChannelFactory $logger,
                              TranslationManager $translationManager,
                              StreamWrapperManager $streamWrapperManager,
                              FileSystem $fileSystem,
                              ConfigFactory $configFactory,
                              EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->translationManager = $translationManager;
    $this->streamWrapperManager = $streamWrapperManager;
    $this->fileSystem = $fileSystem;
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;

    if (isset($configuration['uid']) && isset($configuration['picture'])) {
      $this->referencePicture = $this->getUserPicturePath($configuration['uid']);
      $this->inputPicture = $this->getInputPicturePath($configuration['picture']);
    }
    $this->configuration = array_merge($this->configuration, $this->getFieldConfig());

  }

  /**
   * Base class factory method.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('messenger'),
      $container->get('logger.factory'),
      $container->get('string_translation'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Returns the full path to the user picture.
   */
  protected function getUserPicturePath($uid) {
    $user_account = $this->entityTypeManager->getStorage('user')->load($uid);
    $user_account_picture = $user_account->get('user_picture')->getValue();
    if (isset($user_account_picture[0])) {
      $file = $this->entityTypeManager->getStorage('file')->load($user_account_picture[0]['target_id']);
      $uri = $file->getFileUri();

      return $this->streamWrapperManager->getViaUri($uri)->realpath();
    }

    $log_message = 'Could not perform face recognition because user "@username" is not parametrised with a picture';
    $log_params = ['@username' => $user_account->getAccountName()];

    $this->messenger->addError($this->t($log_message, $log_params));
    $this->logger->get('face_recognition')->debug($log_message, $log_params);

    return FALSE;
  }

  /**
   * Returns the full path to user webcam picture.
   */
  protected function getInputPicturePath($base64_data) {
    $data = base64_decode($base64_data);
    $temp_name = $this->fileSystem->tempnam('temporary://', 'file');
    if (file_put_contents($temp_name, $data) === FALSE) {
      throw new FileWriteException("Temporary file '$temp_name' could not be created.");
    }

    return $this->streamWrapperManager->getViaUri($temp_name)->realpath();
  }

  /**
   * Performs a validation by execution the plugin children method.
   */
  public function validateFace() {
    if (!$this->referencePicture) {
      return FALSE;
    }

    return $this->validate();
  }

  /**
   * Base plugin configuration form method.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if (isset($form[$this->getPluginId()])) {
      $plugin_label = $this->getPluginDefinition()['label'];
      $fieldset = [
        '#type' => 'fieldset',
        '#title' => $plugin_label . ' ' . $this->t('Plugin settings'),
        '#states' => [
          'visible' => [
            ':input[name="active"]' => ['checked' => TRUE],
            ':input[name="plugin"]' => ['value' => $this->getPluginId()],
          ],
        ],
      ];
      foreach ($form[$this->getPluginId()] as $field_name => &$field_item) {
        $field_item['#default_value'] = $this->getFieldConfig($field_name);
        $field_item['#states']['required'] = [':input[name="plugin"]' => ['value' => $this->getPluginId()]];
      }
      $form[$this->getPluginId()] = array_merge($form[$this->getPluginId()], $fieldset);
    }

    return $form;
  }

  /**
   * In case of need it could be implemented on the children plugin.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * In case of need it could be implemented on the children plugin.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Translate method helper.
   */
  protected function t($string, array $args = [], array $options = []) {
    return $this->translationManager->translate($string, $args, $options);
  }

  /**
   * Plugin configuration method helper.
   */
  protected function getFieldConfig($name = '') {
    return $this->configFactory->get('face_recognition.settings')->get($name);
  }

}
