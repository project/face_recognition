<?php

namespace Drupal\face_recognition\Plugin\FaceRecognition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\face_recognition\FaceRecognitionPluginBase;

/**
 * Plugin implementation of the face_recognition.
 *
 * @FaceRecognition(
 *   id = "custom_validation",
 *   label = @Translation("Custom Validation"),
 *   description = @Translation("Based on Artificial Intelligence Program.")
 * )
 */
class CustomValidation extends FaceRecognitionPluginBase {

  /**
   * Validate with a custom solution.
   */
  public function validate() {
    $command = [
      $this->configuration['validation_script_path'],
      $this->configuration['reference_image_parameter'],
      $this->referencePicture,
      $this->configuration['input_image_parameter'],
      $this->inputPicture,
      $this->configuration['treshold_parameter'],
      $this->configuration['treshold_custom'],
    ];
    $command = implode(' ', $command);
    exec($command, $output, $retval);

    if ($retval == 1) {
      $this->messenger->addError('The face can not be detected');
    }
    elseif ($retval == 2) {
      $this->messenger->addError('The face can not be recognised');
    }

    $this->logger->get('face_recognition')->debug($command);
    $this->logger->get('face_recognition')->info(implode(PHP_EOL, $output));

    return $retval == 0;
  }

  /**
   * Adds some plugin configuration in order to make it work.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form[$this->getPluginId()]['validation_script_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Validation Script Path'),
      '#description' => $this->t('For instance: : /usr/bin/python /home/user/recognition.py'),
    ];
    $form[$this->getPluginId()]['reference_image_parameter'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('Input Image Parameter Name'),
      '#description' => $this->t('For instance: -i'),
    ];

    $form[$this->getPluginId()]['input_image_parameter'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('Reference Image Parameter Name'),
      '#description' => $this->t('For instance: -r'),
    ];

    $form[$this->getPluginId()]['threshold_parameter'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('Threshold Parameter Name'),
      '#description' => $this->t('For instance: -t'),
    ];

    $form[$this->getPluginId()]['threshold_custom'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('Threshold'),
      '#description' => $this->t('For instance: 11'),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

}
