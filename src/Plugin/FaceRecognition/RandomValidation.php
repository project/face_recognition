<?php

namespace Drupal\face_recognition\Plugin\FaceRecognition;

use Drupal\face_recognition\FaceRecognitionPluginBase;

/**
 * Plugin implementation of the face_recognition.
 *
 * @FaceRecognition(
 *   id = "random_validation",
 *   label = @Translation("Random Validation"),
 *   description = @Translation("For testing purpose only.")
 * )
 */
class RandomValidation extends FaceRecognitionPluginBase {

  /**
   * Return random result for testing purpose.
   */
  public function validate() {
    return rand(0, 1);
  }

}
