<?php

namespace Drupal\face_recognition\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\face_recognition\FaceRecognitionPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure face_recognition settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Face Recognition plugin manager.
   *
   * @var \Drupal\face_recognition\FaceRecognitionPluginManager
   */
  protected $manager;

  /**
   * The Face Recognition plugin definitions.
   *
   * @var array
   */
  protected $definitions;

  /**
   * Plugin instances.
   *
   * @var array
   */
  protected $pluginInstances;

  /**
   * The Face Recognition plugin manager.
   *
   * @var Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $invalidator;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, FaceRecognitionPluginManager $manager, CacheTagsInvalidator $invalidator) {
    parent::__construct($config_factory);
    $this->manager = $manager;
    $this->invalidator = $invalidator;
    foreach ($this->manager->getDefinitions() as $id => $definition) {
      $this->definitions[$id] = new FormattableMarkup(
        '@label: <span class="description">@description</span>',
        [
          '@label' => $definition['label'],
          '@description' => $definition['description'],
        ]);
      $this->pluginInstances[] = $this->manager->createInstance($id, []);
      ;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.face_recognition'),
      $container->get('cache_tags.invalidator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'face_recognition_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['face_recognition.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Face Recognition is active'),
      '#description' => $this->t('Activate or not the face recognition system.'),
      '#default_value' => $this->config('face_recognition.settings')->get('active'),
    ];

    $form['plugin'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Face Recognition Plugin'),
      '#description' => $this->t('Choose the right plugin to activate the Face Recognition.'),
      '#options' => $this->definitions,
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $this->config('face_recognition.settings')->get('plugin'),
    ];

    foreach ($this->pluginInstances as $plugin) {
      $form = $plugin->buildConfigurationForm($form, $form_state);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->pluginInstances as $plugin) {
      $plugin->validateConfigurationForm($form, $form_state);
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    foreach ($form_state->cleanValues()->getValues() as $field_name => $field_value) {
      $config->set($field_name, $field_value);
    }
    $config->save();

    $this->invalidator->invalidateTags(['config:face_recognition.settings']);

    foreach ($this->pluginInstances as $plugin) {
      $plugin->submitConfigurationForm($form, $form_state);
    }
    parent::submitForm($form, $form_state);
  }

}
