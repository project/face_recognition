<?php

namespace Drupal\face_recognition;

/**
 * Interface for face_recognition plugins.
 */
interface FaceRecognitionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Perform the validation of the user.
   *
   * @return bool
   *   User validation.
   */
  public function validate();

}
