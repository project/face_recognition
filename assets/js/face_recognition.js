(function (Drupal, $) {
  "use strict";
  Drupal.behaviors.faceRecognition = {
    attach: function (context, settings) {
      $(document, context).once('my_behavior ').each( function() {
        const webcamElement = document.getElementById('webcam');
        const canvasElement = document.getElementById('canvas');
        const snapSoundElement = document.getElementById('snapSound');
        const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);

        webcam.start()
          .then(result =>{
            console.log("webcam started");
          })
          .catch(err => {
            console.log(err);
          });

        $('#snap').on('click', function (e){
          e.preventDefault();
          var picture_uri = webcam.snap();
          var picture_base64 = picture_uri.split(',')[1];
          $('input[name=picture]').val(picture_base64);
          $('.face_recognition_login__step2.hidden').removeClass('hidden');
          $('form#user-login-form #edit-submit').removeAttr('disabled').removeClass('is-disabled');
        });
      });
    }
  };

}) (Drupal, jQuery);


